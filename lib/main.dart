import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:travel_app/client_app/model/user.dart';
import 'package:travel_app/localizations.dart';
import 'LocaleHelper.dart';
import 'client_app/statics/bottom_navigation.dart';

void main() => runApp(MyApp());
class MyApp extends StatefulWidget{
  MyAppState createState()=>MyAppState();
}
class MyAppState extends State<MyApp> {
  User user =User();
  bool  getUser = true;
  static StreamController notifier = new StreamController.broadcast();
  final Stream trigger = notifier.stream ;
  StreamSubscription subscription ;
  SpecificLocalizationDelegate _specificLocalizationDelegate=SpecificLocalizationDelegate(Locale("en"));
  @override void initState() {
    super.initState();

    helper.onLocaleChanged = onLocaleChange;
    subscription =  trigger.listen((i){
      print("from stream");
      this.user.locale=i;
      UserLocalStorage.logIn(this.user);
      setState(() {
        helper.onLocaleChanged(Locale(i));

      });
    });

  }
  onLocaleChange(Locale locale){
    setState((){
      _specificLocalizationDelegate = new SpecificLocalizationDelegate(locale);
    });
  }
  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          new FallbackCupertinoLocalisationsDelegate(),
          //app-specific localization
          _specificLocalizationDelegate
        ],
        supportedLocales: [Locale('en'), Locale('ar')],
        locale: _specificLocalizationDelegate.overriddenLocale,
        title: 'Crystal Magazine',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BottomNavigationClass(0),
      );
  }
}
