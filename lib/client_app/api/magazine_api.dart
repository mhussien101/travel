import 'package:travel_app/client_app/model/news_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class MagazineApi {
  static const String URL = "https://crystal-magazine.com/";
  static Future<MagazineServerResponse> getHomeNews() async {
    String url = URL + "api/pdf";
    print(url);
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    print(jsonResponse);
    return MagazineServerResponse.fromJson(jsonResponse);
  }
}