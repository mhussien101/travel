import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:travel_app/client_app/model/news_model.dart';
class TrendApi {
  static const String URL = "https://crystal-magazine.com/";
  static Future<TrendServerResponse> getAllTrending() async {
    String url = URL + "api/tranding";
    print(url);
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    print(jsonResponse);
    return TrendServerResponse.fromJson(jsonResponse);
  }
}