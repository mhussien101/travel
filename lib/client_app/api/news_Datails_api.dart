import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:travel_app/client_app/model/news_model.dart';
class NewsDetailsApi{
  static const String URL = "https://crystal-magazine.com/";
  static Future<NewsDetailsServerResponse> getNewsDetails(int id) async {
    String url = URL + "api/Products/productDetials/$id";
    print(url);
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    print(jsonResponse);
    return NewsDetailsServerResponse.fromJson(jsonResponse);
  }
}