import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:travel_app/client_app/model/category_model.dart';
import 'package:travel_app/client_app/model/news_model.dart';
class CategoryApi {
  static const String URL = "https://crystal-magazine.com/";
  static Future<GetAllCategoryServerResponse> getAllCategories() async {
    String url = URL + "api/Category/allCategory";
    print(url);
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    print(jsonResponse);
    return GetAllCategoryServerResponse.fromJson(jsonResponse);
  }
  static Future<CategoryNewsByIdResponse> getAllCategoryNewsById(int id) async {
    String url = URL + "api/Products/productByCat/$id";
    print(url);
    final response = await http.get(url);
    final jsonResponse = json.decode(response.body);
    print(jsonResponse);
    return CategoryNewsByIdResponse.fromJson(jsonResponse);
  }
}