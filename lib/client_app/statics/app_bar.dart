import 'package:travel_app/client_app/statics/travel_news_icons.dart';
import 'bottom_navigation.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:flutter/material.dart';

class AppBarClass {
  Widget appBar(
      BuildContext context, GlobalKey<ScaffoldState> _scaffoldKeyProfile,
      {String title}) {
    return PreferredSize(
      preferredSize: Size.fromHeight(70.0),
      child: AppBar(
        backgroundColor:TravelNewsColors.blue56,
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.only(top:8.0),
          child: Text(
            title == null ? 'Crystal Magazine' : title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontFamily: "Poppins-Bold",
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(left:16.0,top: 8),
          child: IconButton(
              icon: Icon(
                Icons.dehaze,
                size: 40,
                color: Colors.white,
              ),
              onPressed: () {
                _scaffoldKeyProfile.currentState.openDrawer();
              }),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right:16.0,top: 8),
            child: Icon(
              Icons.search,
              size: 40,
            ),
          ),
        ],
      ),
    );
  }
}
