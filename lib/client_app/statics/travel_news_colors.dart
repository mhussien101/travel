import 'package:flutter/material.dart';
class TravelNewsColors{
  static final Color blue56 =Color(0xff56C0CD);
  static final Color blueB3 =Color(0xffB3E3E8);
  static final Color blueC6 =Color(0xffC6EAEE);
  static final Color grey7E =Color(0xff7E7E7E);
  static final Color greyE5 =Color(0xffE5E5E5);
}