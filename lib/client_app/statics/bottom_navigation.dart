import 'package:flutter/material.dart';
import 'package:travel_app/client_app/model/user.dart';
import 'package:travel_app/client_app/statics/app_bar.dart';
import 'package:travel_app/client_app/statics/drawer.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/client_app/ui/categories.dart';
import 'package:travel_app/client_app/ui/home.dart';
import 'package:travel_app/client_app/ui/saved.dart';
import 'package:travel_app/client_app/ui/trends.dart';
import 'package:travel_app/localizations.dart';

import '../../main.dart';
class BottomNavigationClass extends StatefulWidget{
  int initial ;
  BottomNavigationClass(this.initial);
  _BottomNavigationClassState createState()=> _BottomNavigationClassState();
}
class _BottomNavigationClassState extends State<BottomNavigationClass>{

  User user ;
  bool lang = false ;
  int _page =0;
  GlobalKey<ScaffoldState> _globalKeyScafoldState ;
  PageController _pageController;
  getUserfn() {
    setState(() {
      lang =true;
    });
    UserLocalStorage.getUser().then((user) {
      this.user = user;
      print("from get user fn");
      if (this.user.locale != null) {
        print("if condition");
        MyAppState.notifier.sink.add(this.user.locale);
        setState(() {
          lang =false;
        });
      }
      else{
        print("else condition");
        setState(() {
          lang =false;
        });
      }

    },onError: (e){
      setState(() {
        lang =false;
      });
      print("error :::::: : :: :: :  $e");
    });
  }
  navigationTapped(int page) {
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 100), curve: Curves.ease);
  }
  void onPageChanged(int page){
    print(page);
    setState(() {
      this._page = page;
    });
  }
  @override
  void initState() {
    getUserfn();
    super.initState();
    _globalKeyScafoldState = new GlobalKey<ScaffoldState>();
    _pageController = new PageController();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => navigationTapped(widget.initial));

  }
  @override
  void dispose() {
    super.dispose();
    print("bottom navigation Disposed ");
  }
  @override
  Widget build(BuildContext context) {
    BottomNavigationBarItem home = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
                width: 24.0,
                height: 24.0,
                child:_page == 0
                    ? Image.asset("images/bottom_home_active.png")
                    :Image.asset("images/bottom_home.png"),
            ),
            new Container(height: 4.0),
            new Text(AppLocalizations.of(context).locale=='en'?"Home":"الرئيسية",
                style: new TextStyle(
                  color: _page == 0 ? TravelNewsColors.blue56:TravelNewsColors.grey7E,
                  fontSize: 12.0,
                ))
          ],
        )
    );
    BottomNavigationBarItem trends = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right:12.0,bottom: 10),
              child: Container(
                width: 24.0,
                height: 24.0,
                child: Align(
                  alignment: Alignment.topRight,
                    child: new Container(
                        width: 24.0,
                        height: 24.0,
                      child: _page == 1
                          ? Image.asset("images/bottom_trand_active.png")
                          :Image.asset("images/bottom_trend.png"),
                    ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom:04.0,right: 12),
              child: new Text(AppLocalizations.of(context).locale=='en'?"Trend":"أكثر رواجا",
                textAlign: TextAlign.left,
                style: new TextStyle(
                  color: _page == 1 ? TravelNewsColors.blue56:TravelNewsColors.grey7E,
                  fontSize: 12.0,
                ),
              ),
            ),
          ],
        ));
    BottomNavigationBarItem categories = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
              width: 24.0,
              height: 24.0,
              child: _page == 2
                  ? Image.asset("images/bottom_cat_active.png",width: 24,height: 24,)
                  :Image.asset("images/bottomSheet_cat_icon.png",width: 24,height: 24,),
            ),
            new Container(height: 4.0),
            new Text(AppLocalizations.of(context).locale=='en'?"Categories":"الفئات",
                style: new TextStyle(
                  color: _page == 2 ? TravelNewsColors.blue56:TravelNewsColors.grey7E,
                  fontSize: 12.0,
                ))
          ],
        ));
    BottomNavigationBarItem saved = new BottomNavigationBarItem(
        title: new Container(),
        icon: new Column(
          children: <Widget>[
            new Container(
              width: 24.0,
              height: 24.0,
              child: _page ==3
                  ? Image.asset("images/bottom_save_active.png")
                  :Image.asset("images/bottom_save.png"),
            ),
            new Container(height: 4.0),
            new Text(AppLocalizations.of(context).locale=='en'?"Saved":"الحافظة",
                style: new TextStyle(
                  color: _page == 3 ? TravelNewsColors.blue56:TravelNewsColors.grey7E,
                  fontSize: 12.0,
                ))
          ],
        ));
    Widget bottomNavigationBarWidget = BottomNavigationBar(
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      items: [
        home,
        trends,
        categories,
        saved,
      ],
      onTap: navigationTapped,
      currentIndex: _page,
    );
    return lang? Material(
      color: Color(0xfff0f0f0),
        child :Center(
          child: CircularProgressIndicator(),
        ),
    ): Scaffold(
     // backgroundColor: Color(ExpressColors.SECONDARY_COLOR),
      drawer: DrawerClass().showDrawer(context),
      key: _globalKeyScafoldState,
      bottomNavigationBar: bottomNavigationBarWidget,
      appBar: new AppBarClass().appBar(context ,_globalKeyScafoldState,),//.appBar(this.context,_globalKeyScafoldState,count),
      body: new PageView(
          scrollDirection: Axis.horizontal,
          pageSnapping: true,
          reverse: false,
          physics: const BouncingScrollPhysics(),
          children: [
            Home(),
            Trends(),
            Categories(),
            Saved(),
          ],
          controller: _pageController,
          onPageChanged: onPageChanged),
    );
  }
}