import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travel_app/client_app/model/user.dart';
import 'package:travel_app/client_app/statics/travel_news_icons.dart';
import 'package:travel_app/client_app/ui/about_us.dart';
import 'package:travel_app/client_app/ui/contact_us.dart';
import 'package:travel_app/client_app/ui/magazine.dart';
import 'package:travel_app/localizations.dart';
import 'package:travel_app/main.dart';
import 'bottom_navigation.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
class DrawerClass{
  User user ;
  Widget showDrawer(BuildContext context ) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width/1.25,
      child: Column(
        children: <Widget>[
          Flexible(
            child: Container(
              color: TravelNewsColors.blue56,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: MediaQuery.of(context).size.width<340?<Widget>[
                  Container(
                    height: 30,
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      //padding: EdgeInsets.only(top:MediaQuery.of(context).size.width<340?25: 60),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top:0.0,left: 16 ,right: 16),
                            child: CircleAvatar(
                              radius: MediaQuery.of(context).size.width<340?35:50,
                              backgroundColor: Colors.white,
                              child: IconButton(
                                icon: Icon(Icons.person,
                                  size: MediaQuery.of(context).size.width<340?35:50,
                                  color: TravelNewsColors.blue56,
                                ),
                                alignment: Alignment.center,
                                iconSize: MediaQuery.of(context).size.width<340?35:50,
                                onPressed: (){},
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topRight,
                              child: Padding(
                                padding: const EdgeInsets.only(top:0.0,right: 16,left: 16),
                                child: Icon(
                                  Icons.close,
                                  color: TravelNewsColors.blueC6,
                                  size: 30,
                                ),
                              ),
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                    ),
                    flex: MediaQuery.of(context).size.width<340?1:2,
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left :16.0 ,top : 0),
                                child: Text("Welcome to",
                                  style: TextStyle(
                                      fontFamily: "Poppins-Bold",
                                      color: Colors.white,
                                      fontSize: MediaQuery.of(context).size.width<340?12:16
                                  ),
                                ),
                              ),
                              flex: 4,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left :16.0 , top: 0,bottom: 0),
                          child: Row(
                            children: <Widget>[
                              Text("Crystal Magazine",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width<340?16:20,
                                  fontFamily: "Poppins-Bold",
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    flex:1,
                  ),

                ]:<Widget>[
                  Flexible(
                    fit: FlexFit.loose,
                    child: Container(
                      height: 26,
                    ),
                    flex: 1,
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      //padding: EdgeInsets.only(top:MediaQuery.of(context).size.width<340?25: 60),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top:0.0,left: 16 ,right: 16),
                            child: CircleAvatar(
                              radius: MediaQuery.of(context).size.width<340?35:50,
                              backgroundColor: Colors.white,
                              child: IconButton(
                                icon: Icon(Icons.person,
                                  size: MediaQuery.of(context).size.width<340?35:50,
                                  color: TravelNewsColors.blue56,
                                ),
                                alignment: Alignment.center,
                                iconSize: MediaQuery.of(context).size.width<340?35:50,
                                onPressed: (){},
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment:AppLocalizations.of(context).locale=="en"?
                              Alignment.topRight:Alignment.topLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(top:0.0,right: 16,left: 16),
                                child: Icon(
                                  Icons.close,
                                  color: TravelNewsColors.blueC6,
                                  size: 30,
                                ),
                              ),
                            ),
                            flex: 1,
                          ),
                        ],
                      ),
                    ),
                    flex: MediaQuery.of(context).size.width<340?1:3,
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left :16.0 ,top : 0,right: 16),
                                  child: Text("${AppLocalizations.of(context).drawerWelcome}",
                                    style: TextStyle(
                                        fontFamily: "Poppins-Bold",
                                        color: Colors.white,
                                        fontSize: MediaQuery.of(context).size.width<340?12:16
                                    ),
                                  ),
                                ),
                                flex: 4,
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left :16.0,right: 16 ),
                            child: Row(
                              children: <Widget>[
                                Text("${AppLocalizations.of(context).drawerTravel}",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: MediaQuery.of(context).size.width<340?16:20,
                                    fontFamily: "Poppins-Bold",
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    flex:2,
                  ),
                ],
              ),
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.only(left: 8,right: 8,top: 8,bottom: 8),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child:new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerHome}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      leading: Icon(TravelNews.bottom_navigation_home),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BottomNavigationClass(0)));
                      },
                    ),
                  ),

                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerSave}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      //TODO : شوفلها علاج
                      leading: Image.asset("images/drawer_save.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BottomNavigationClass(3)));
                      },
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerCategories}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      leading:Image.asset("images/bottomSheet_cat_icon.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BottomNavigationClass(2)));
                      },
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    fit: FlexFit.loose,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8,),
                      child: Divider(
                        endIndent: MediaQuery.of(context).size.width<340?65:100,
                        indent: 16,
                        color: Color(0xff707070),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child:new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerMagazine}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      leading: Image.asset("images/drawer_issue.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Magazine())) ;
                      },
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child:new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerAboutUs}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      leading: Image.asset("images/drawer_about.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AboutUs())) ;
                      },
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerContactUs}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),

                      leading: Image.asset("images/drawer_contact.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ContactUs())) ;
                      },
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text(
                        "${AppLocalizations.of(context).drawerShare}",
                        style: new TextStyle(
                          fontSize: 16.0,
                          color: TravelNewsColors.grey7E,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      leading:Image.asset("images/drawer_share.png",width: 24,height: 24,),
                      onTap: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context)=>OrdersPage())) ;
                      },
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(top:8.0),
                      child: Divider(
                        endIndent: MediaQuery.of(context).size.width<340?65:100,
                        indent: 16,
                        color: Color(0xff707070),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: Row(
                        children: <Widget>[
                          InkWell(
                            child: Text("English   ",
                              style: TextStyle(
                                fontSize: 14,
                                fontFamily: "Poppins-Medium",
                                color: AppLocalizations.of(context).locale=="en"?
                                Color(0xffDEB1D0):TravelNewsColors.grey7E,
                              ),
                            ),
                            onTap: (){

                              MyAppState.notifier.sink.add("en");
                              print("en tapped");
                              Navigator.of(context).pop();

                            },
                          ),
                          Container(
                            width: 2,
                            height: 16,
                            color: TravelNewsColors.grey7E,
                          ),
                          InkWell(
                            onTap: (){
                              MyAppState.notifier.sink.add("ar");
                              print("ar tapped");
                              Navigator.of(context).pop();
                            },
                            child: Text("    عربى",
                              style: TextStyle(
                                fontFamily: "Poppins-Bold",
                                color: AppLocalizations.of(context).locale=="ar"?Color(0xffDEB1D0)
                                    :TravelNewsColors.grey7E,
                                fontSize: 15,
                                height: 1
                              ),
                            ),
                          ),
                        ],
                      ),
                      leading: Image.asset("images/drawer_lang.png",width: 24,height: 24,),
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Container(),
                  ),
                ],
              ),
            ),
            flex: 5,
          ),
        ],
      ),
    );
  }
}
class ChooseLanguage extends StatefulWidget{
  _ChooseLanguageState createState()=>_ChooseLanguageState();
}
class _ChooseLanguageState extends State<ChooseLanguage>{
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        width: 200,height: 300,
        child: Center(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            child: Center(
              child:Column(
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text("العربية",
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: Colors.grey[50],
                            fontFamily: 'JF Flat'),
                      ),
                      onTap: () {
                        MyAppState.notifier.sink.add("ar");
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: new ListTile(
                      title: new Text("English",
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: Colors.grey[50],
                            fontFamily: 'JF Flat'),
                      ),
                      onTap: () {
                        MyAppState.notifier.sink.add("en");
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}