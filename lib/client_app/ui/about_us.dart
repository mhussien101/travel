import 'package:flutter/material.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';

import '../../localizations.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFF5F5F5),
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right:12.0,top: 8),
            child: Icon(Icons.search,
              size: size.width*0.09,
            ),
          ),
        ],
        backgroundColor: TravelNewsColors.blue56,
        centerTitle: true,
        title: Text(AppLocalizations.of(context).locale=='en'?'About Us':"حول",
          style: TextStyle(
            fontSize: size.width*0.059,
            color: Colors.white,
          ),
        ),
      ),
      body:SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top :4.0),
                  child: Container(
                    height: size.height/3.5,
                    width: size.width/1.5,
                      child: Image.asset("images/about.png",fit: BoxFit.fill,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:32.0),
                  child: Text(AppLocalizations.of(context).locale=='en'?
                  "Who are we ?":"من نحن ؟",
                    style: TextStyle(
                      fontSize: size.width*0.065,
                      fontFamily:'Poppins-Bold'
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:14.0),
                  child: Container(
                    width: size.width,
                    child: Text(AppLocalizations.of(context).locale=='en'?"Seamlessly communicate, gather feedback, "
                        "and move projects forward Seamlessly communicate, "
                        "gather feedback, and move projects forward Seamlessly communicate, "
                        "gather feedback, and move projects forward":
                    "التواصل بسلاسة وجمع التعليقات ونقل المشاريع إلى الأمام التواصل بسلاسة "
                        "وجمع التعليقات ونقل المشاريع إلى الأمام الاتصال بسلاسة وجمع التعليقات ونقل المشاريع للأمام",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: TravelNewsColors.grey7E,
                          fontSize: size.width*0.046,
                          fontFamily:'Poppins-Medium',
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:32.0),
                  child: Text(AppLocalizations.of(context).locale=='en'?
                  "Our Services":"خدماتنا",
                    style: TextStyle(
                        fontSize: size.width*0.065,
                        fontFamily:'Poppins-Bold'
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:14.0),
                  child: Container(
                    width: size.width,
                    child: Text(AppLocalizations.of(context).locale=='en'?
                    "1- Websites\n2- Mobile Apps\n3- Client":"1- مواقع إلكترونية \n 2- تطبيقات موبايل \n3- العميل",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: TravelNewsColors.grey7E,
                        fontSize: size.width*0.046,
                        fontFamily:'Poppins-Medium',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
