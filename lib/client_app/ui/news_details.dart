import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:travel_app/client_app/api/news_Datails_api.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/model/tags_model.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/localizations.dart';
class NewsDetails extends StatefulWidget{
  int id;
  NewsDetails(this.id);
  _NewsDetailsState createState()=> _NewsDetailsState();
}
class _NewsDetailsState extends State<NewsDetails> {
  bool popular1 =false;
  bool getNewsDetailsApiCall =false;
  ScrollController _scrollViewController;
  NewsModel news ;
  List<NewsModel> similarList = new List();
  List<Widget> similarItemsList = List();
  List<TagsModel> tagsList = List();
  getNewsDetails(){
    setState(() {
      getNewsDetailsApiCall =true;
    });
    NewsDetailsApi.getNewsDetails(widget.id).then((response){
      news = response.news;
      tagsList = response.tagsList;
      similarList = response.similarList;
      similarList.forEach((item)=>similarItemsList.add(SimilarItem(item)));
      setState(() {
        getNewsDetailsApiCall =false;
      });
    },onError: (error){
      setState(() {
        getNewsDetailsApiCall =false;
      });
      print("News Details error : : :   $error");
    });
  }
  @override
  void initState() {
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    super.initState();
    this.getNewsDetails();
  }
  @override
  Widget build(BuildContext context) {
    return  Material(
      child: getNewsDetailsApiCall?Center(child: CircularProgressIndicator(),):
      SingleChildScrollView(
        controller: _scrollViewController,
        physics: BouncingScrollPhysics(),
        child: Container(
            color: Color(0xFFF5F5F5),
            child: Column(
              children: <Widget>[
                Container(
                  height:MediaQuery.of(context).size.width<340?24: 24,
                ),
                Container(
                  color: Color(0xFFF5F5F5),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/3.8,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        top:0,bottom: 0,left: 0,right: 0,
                        child: Image.network(news.image,fit: BoxFit.fill,),//Image.asset("images/details_plane.png",fit: BoxFit.fill,),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.arrow_back,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            padding: EdgeInsets.only(left: 16),
                            iconSize: 40,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right:16.0),
                            child: InkWell(
                              onTap: (){
                                setState(() {
                                  popular1=!popular1;
                                });
                              },
                              child: popular1?Image.asset("images/home_popular_save_active.png",width: 24,height: 24,)
                                  :Image.asset("images/home_popular_save.png",width: 24,height: 24,),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                tagsList.length>0?Container(
                  padding: AppLocalizations.of(context).locale=='en'?
                  EdgeInsets.only(left: 0,top: 14,right: 90):EdgeInsets.only(right: 0,top: 14,left: 90),
                  height: MediaQuery.of(context).size.height/15,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount:tagsList.length,
                        itemBuilder: (context,position){
                          return Text(AppLocalizations.of(context).locale=='en'?
                          "${tagsList[position].name}":"${tagsList[position].name_ar}",
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.width<340?12:14,
                              fontFamily: 'Poppins-Medium',
                              color: TravelNewsColors.blue56,
                            ),
                          );
                        },)

                    ],
                  ),
                ):Container(
                  color: Color(0xFFF5F5F5),
                  padding: EdgeInsets.only(top: 8),
                ),
                Container(
                  padding: EdgeInsets.only(left: 16,bottom: 6,right: 16),
                  color: Color(0xFFF5F5F5),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding:EdgeInsets.only(top: 4,bottom: 4,left: 8,right: 8),
                      decoration: BoxDecoration(
                        color: TravelNewsColors.blue56,
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Center(
                        child: Text("${news.news_paper}",//"CNN",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Poppins-SemiBold",
                            fontSize: MediaQuery.of(context).size.width<340?10:12,
                          ),
                        ),
                      ),
                    ),
                    Text("    ${news.created_at.substring(0,11)}",//September 2, 2019",
                      style: TextStyle(
                        fontSize:MediaQuery.of(context).size.width<340?10: 12,
                        color: Colors.black87,
                        fontFamily: "Poppins-Regular",
                      ),
                    ),
                  ],
                ),
                ),
                Container(
                  color: Color(0xFFF5F5F5),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 16,right: 16,bottom: 6),
                  child: Material(
                    child: Text(AppLocalizations.of(context).locale=='en'?
                    "${news.name}":"${news.name_ar}",
                      textAlign: AppLocalizations.of(context).locale=='en'?
                      TextAlign.left:TextAlign.right, //"10 PLACES EVERYONE WANTS TO TRAVEL TO IN 2019",
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width<340?20:24,
                        fontFamily: 'Poppins-Bold',
                      ),
                    ),
                  ),
                ),
                Container(
                  color: Color(0xFFF5F5F5),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 16,right: 16,bottom: 16),
                  child: Material(
                    child: Text(AppLocalizations.of(context).locale=='en'?"${news.desc}":"${news.desc_ar}",
                      textAlign: AppLocalizations.of(context).locale=='en'?TextAlign.left:TextAlign.right,
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width<340?12:16,
                        fontFamily: 'Poppins-SemiBold',
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 16,bottom: 12,right: 16),
                  child: Row(
                    children: <Widget>[
                      Text(AppLocalizations.of(context).locale=='en'?
                      "similar news":"أخبار مشابهة",
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width<340?14:18,
                          fontFamily: 'Poppins-Bold',
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 16,right: 16,top: 0),
                  //height: MediaQuery.of(context).size.height-150,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children:similarItemsList.length>0?similarItemsList : Container(),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}
class SimilarItem extends StatefulWidget {
  NewsModel news ;
  SimilarItem(this.news);
  @override
  _SimilarItemState createState() => _SimilarItemState();
}

class _SimilarItemState extends State<SimilarItem> {
  bool new1 = false;
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context)=>NewsDetails(widget.news.id)
          ));
        },
        child: ClipRRect(
          borderRadius:BorderRadius.circular(4),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              height:MediaQuery.of(context).size.width<365?135:MediaQuery.of(context).size.width<415?150:164,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height: MediaQuery.of(context).size.width<365?135:MediaQuery.of(context).size.width<415?150:164,
                        child: Image(
                          fit: BoxFit.fill,
                          image: NetworkImageWithRetry(widget.news.image),
                        ),//Image.network(widget.news.image),//Image.asset("images/listhome1.png",fit: BoxFit.fitHeight,),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            flex:3,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left:8.0,bottom: 0,top: 8,right: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding:EdgeInsets.only(top: 2,bottom: 0,left: 6,right: 6),
                                        decoration: BoxDecoration(
                                          color: TravelNewsColors.blue56,
                                          borderRadius: BorderRadius.circular(4),
                                        ),
                                        child: Center(
                                          child: Text("${widget.news.news_paper}",//"CNN",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "Poppins-SemiBold",
                                              fontSize: MediaQuery.of(context).size.width<365?8:10,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text("    ${widget.news.created_at.substring(0,11)}",//September 2,2019",
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context).size.width<365?8:10,
                                          color: Colors.black,
                                          fontFamily: "Poppins-Regular",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: AppLocalizations.of(context).locale=='en'?
                                    EdgeInsets.only(top:8.0,right: 8):EdgeInsets.only(top: 8,left: 8),
                                    child: Align(
                                      alignment: AppLocalizations.of(context).locale=='en'?
                                      Alignment.topRight:Alignment.topLeft,
                                      child: InkWell(
                                          onTap: (){
                                            setState(() {
                                              new1=!new1;
                                            });
                                          },
                                          child: new1?Image.asset("images/home_news_save_active.png",width: 24,height: 24,)
                                              :Image.asset("images/home_news_save.png",width: 24,height: 24,)
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 3,
                            fit: FlexFit.loose,
                            child: Padding(
                              padding: const EdgeInsets.only(left:8.0,top: 4,right: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Text(AppLocalizations.of(context).locale=='en'?
                                    "${widget.news.name}":"${widget.news.name_ar}",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,//"Maui, Hawaii — September is the cheapest time to go",
                                      style: TextStyle(
                                        fontSize: MediaQuery.of(context).size.width<365?12:15,
                                        fontFamily: "Poppins-Bold",
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 6,
                            fit: FlexFit.tight,
                            child:Padding(
                              padding: const EdgeInsets.only(left:8.0,top: 2,right: 8,bottom: 4),
                              child: Text(AppLocalizations.of(context).locale=='en'?
                              "${widget.news.desc}":"${widget.news.desc_ar}",
                                style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width<365?10:12,
                                  fontFamily: "Poppins-Regular",
                                ),
                                maxLines: MediaQuery.of(context).size.width<340?2:3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
