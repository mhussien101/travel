import 'package:flutter/material.dart';
import 'package:travel_app/client_app/api/trending_api.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/client_app/ui/news_details.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:travel_app/localizations.dart';
class Trends extends StatefulWidget{
  _TrendsState createState()=>_TrendsState();
}
class _TrendsState extends State<Trends>{
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  bool trendApiCall = false ;
  List<NewsModel> trendItemsList = new List();
  void _onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    print("on Refresh : : ");
    this.getTrendNews();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
  getTrendNews(){
    setState(() {
      trendApiCall = true ;
    });
    TrendApi.getAllTrending().then((response){
      trendItemsList =response.trendingList;
      setState(() {
        trendApiCall = false ;
      });
    },onError: (error){
      setState(() {
        trendApiCall = false ;
      });
      print("Trends Error :: $error");
    });

  }
  @override
  void initState() {
    super.initState();
    this.getTrendNews();
  }
  @override
  Widget build(BuildContext context) {
    return trendApiCall?Center(child:CircularProgressIndicator()):
        Container(
          color: Color(0xFFF5F5F5),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(top: 16,bottom: 0,left: 16,right: 16),
                      child: Text(AppLocalizations.of(context).locale=='en'?
                      "Trend news":"الأخبار الأكثر شيوعا",
                        textAlign: AppLocalizations.of(context).locale=='en'?
                        TextAlign.left:TextAlign.right,
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width<340?12:16,
                          fontFamily: "Poppins-Bold",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: MediaQuery.of(context).size.width<340?7:12,
                fit: FlexFit.tight,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(left:16.0,right: 16,top: 12),
                    child: SmartRefresher(
                      controller: _refreshController,
                      onRefresh: _onRefresh,
                      //enablePullUp: true,
                      enablePullDown: true,
                      child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: trendItemsList.length,
                          itemBuilder: (context,position){
                            return TrendItem(trendItemsList[position]);
                          }),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );

  }
}
class TrendItem extends StatefulWidget{
  NewsModel news ;
  TrendItem(this.news);
  _TrendItemState createState()=>_TrendItemState();
}
class _TrendItemState extends State<TrendItem>{
  bool popular1 = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom:16.0),
      child: InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context)=>NewsDetails(widget.news.id)
          ));
        },
        child: ClipRRect(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height<685?145:MediaQuery.of(context).size.height/5,
            child: Stack(
              children: <Widget>[
                Container(
                  width:  MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height<685?145:MediaQuery.of(context).size.height/5,
                  child: Image.network(widget.news.image,fit: BoxFit.cover),//Image.asset("images/trend${position+1}.png",fit: BoxFit.cover,),
                ),
                Container(
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: Alignment.topRight,
                        stops: [0.1, 0.4,0.8],
                        colors: [
                          Colors.black87,
                          Colors.black45,
                          Colors.black12,
                        ],
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Align(
                                alignment:AppLocalizations.of(context).locale=='en'?
                                Alignment.topRight:Alignment.topLeft,
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        popular1=!popular1;
                                      });
                                    },
                                    child: popular1?Image.asset("images/home_popular_save_active.png",width: 24,height: 24,)
                                        :Image.asset("images/home_popular_save.png",width: 24,height: 24,)
                                )
                            ),
                          ),
                        ),
                        Padding(
                          padding: AppLocalizations.of(context).locale=='en'?
                          EdgeInsets.only(left:16.0,bottom: 2):EdgeInsets.only(right: 16,bottom: 2),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding:EdgeInsets.only(top: 4,bottom: 4,left: 8,right: 8),
                                decoration: BoxDecoration(
                                  color: TravelNewsColors.blue56,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Center(
                                  child: Text("${widget.news.news_paper}",//"CNN",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "Poppins-SemiBold",
                                      fontSize: MediaQuery.of(context).size.width<340?8:10,
                                    ),
                                  ),
                                ),
                              ),
                              Text("    ${widget.news.created_at.substring(0,11)}",//September 2,2019",
                                style: TextStyle(
                                  fontSize:MediaQuery.of(context).size.width<340?8: 10,
                                  color: Colors.white,
                                  fontFamily: "Poppins-Regular",
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding:
                          EdgeInsets.only(left:16.0,bottom: 16,right: 16),
                          child: Container(
                            child: Text(AppLocalizations.of(context).locale=='en'?
                            "${widget.news.name}":"${widget.news.name_ar}",//"10 PLACES EVERYONE WANTS TO\nTRAVEL TO IN 2019",
                              textAlign: AppLocalizations.of(context).locale=='en'?
                              TextAlign.left:TextAlign.right,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width<340?14:18,
                                fontFamily: "Poppins-Bold",
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }

}