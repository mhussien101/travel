import 'dart:math';

import 'package:flutter/material.dart';
import 'package:travel_app/client_app/model/NewsDatabaseProvider.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import '../../localizations.dart';
import 'news_details.dart';

class Saved extends StatefulWidget {
  _SavedState createState() => _SavedState();
}

class _SavedState extends State<Saved> {
  List<Widget> latestItemsList= new List();

  void onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    latestItemsList.clear();
    print("on Refresh : : ");
    this.getHomeNews();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF5F5F5),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child:SingleChildScrollView(
        child: Container(
            color: Color(0xFFF5F5F5),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only( top: 16),
                  child: Column(
                    children: <Widget>[
                      Text(
                        AppLocalizations
                            .of(context)
                            .locale ==
                            'en'
                            ? "Saved"
                            : 'المحفوظ',
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins-Bold",
                          color: Colors.black,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left:16,right: 16,bottom: 16, top: 16),
                        child: Column(
                          children: latestItemsList,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
  @override
  void initState() {
    super.initState();
    this.getHomeNews();
  }

  getHomeNews() {
    TodoProvider databaseProvider = new TodoProvider();
    databaseProvider.open().then((response){
      print("getSavedData/ done");
      databaseProvider.getSavedData().then((response){
        print("getSavedData/response/length" + response.length.toString());
        response.forEach((latest)=>latestItemsList.add(SavedItem(latest, () {
          setState(() {
            print("onRefresh/ ok");
          //  onRefresh();
          });
        })));
        print("bjknlgtmjnjkgnkbg");
        setState(() {});
      },onError: (error){
        print("get saved news error :: :  $error");
      });
    },onError: (error){
      print(" open database error :: :  $error");
    });
  }

}

class SavedItem extends StatefulWidget {
  int position;
  NewsModel news;
  final VoidCallback onPressed;

  SavedItem(this.news, this.onPressed);

  @override
  _SavedItemState createState() => _SavedItemState();
}

class _SavedItemState extends State<SavedItem> {
  bool new1 = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
              builder: (context)=>NewsDetails(widget.news.id)
          ));
        },
        child: ClipRRect(
          borderRadius:BorderRadius.circular(4),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              height: MediaQuery.of(context).size.width<365?120:150,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height: MediaQuery.of(context).size.width<365?120:150,
                        child: Image.network(widget.news.image,fit: BoxFit.cover),//Image.asset("images/listhome1.png",fit: BoxFit.cover,),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            flex:3,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding:AppLocalizations.of(context).locale=='en'?
                                  const EdgeInsets.only(left:8.0,bottom: 0,top: 8):EdgeInsets.only(right: 8,top:8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding:EdgeInsets.only(top: 2,bottom: 0,left: 6,right: 6),
                                        decoration: BoxDecoration(
                                          color: TravelNewsColors.blue56,
                                          borderRadius: BorderRadius.circular(4),
                                        ),
                                        child: Center(
                                          child: Text("${widget.news.news_paper}",//"CNN",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "Poppins-SemiBold",
                                              fontSize: MediaQuery.of(context).size.width<365?8:10,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text("    ${widget.news.created_at.substring(0,11)}",//September 2,2019",
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context).size.width<365?8:10,
                                          color: Colors.black,
                                          fontFamily: "Poppins-Regular",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: AppLocalizations.of(context).locale=='en'?
                                    EdgeInsets.only(top:8.0,right: 8):EdgeInsets.only(top: 8,left: 8),
                                    child: Align(
                                      alignment: AppLocalizations.of(context).locale=='en'?
                                      Alignment.topRight:Alignment.topLeft,
                                      child: InkWell(
                                          onTap: (){
                                            TodoProvider databaseProvider = new TodoProvider();
                                            databaseProvider.open().then((response){
                                              databaseProvider.delete(widget.news.id).then((onValue){
                                                print(" delete item from database done");
                                                widget.onPressed;
                                                setState(() {
                                                  new1=!new1;
                                                });
                                              },onError: (error){
                                                print(" delete item from database error :: :  $error");
                                              });
                                            },onError: (error){
                                              print(" open database error :: :  $error");
                                            });
                                          },
                                          child: new1?Image.asset("images/home_news_save_active.png",width: 24,height: 24,)
                                              :Image.asset("images/home_news_save.png",width: 24,height: 24,)
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 3,
                            fit: FlexFit.loose,
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(left:8.0,top: 2,right: 8),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        child: Text(AppLocalizations.of(context).locale=='en'?
                                        "${widget.news.name}":"${widget.news.name_ar}",
                                          textAlign: AppLocalizations.of(context).locale=='en'?
                                          TextAlign.left:TextAlign.right,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,//"Maui, Hawaii — September is the cheapest time to go",
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context).size.width<365?12:16,
                                            fontFamily: "Poppins-Bold",
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 6,
                            fit: FlexFit.tight,
                            child:Padding(
                              padding: const EdgeInsets.only(left:8.0,top: 0,right: 8,bottom: 2),
                              child: Text(AppLocalizations.of(context).locale=='en'?
                              "${widget.news.desc}":"${widget.news.desc_ar}",
                                textAlign: AppLocalizations.of(context).locale=='en'?TextAlign.left:TextAlign.right,
                                style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width<365?10:12,
                                  fontFamily: "Poppins-Regular",
                                ),
                                maxLines: MediaQuery.of(context).size.width<340?2:3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
