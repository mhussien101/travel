import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:travel_app/client_app/api/category_api.dart';
import 'package:travel_app/client_app/model/category_model.dart';
import 'package:travel_app/client_app/ui/category_news.dart';
import 'package:travel_app/localizations.dart';
class Categories extends StatefulWidget{
  _CategoriesState createState()=>_CategoriesState();
}
class _CategoriesState extends State<Categories>{
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  List<CategoryModel> categoryList = List();
  bool getCategoriesApiCall = false ;
  void _onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    print("on Refresh : : ");
    this.getAllCategories();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
  getAllCategories(){
    setState(() {
      getCategoriesApiCall = true ;
    });
    CategoryApi.getAllCategories().then((response){
      categoryList = response.categoryList;
      setState(() {
        getCategoriesApiCall = false ;
      });
    },onError: (error){
      setState(() {
        getCategoriesApiCall = false ;
      });
      print("Categories error : : $error");
    });
  }
  @override
  void initState() {
    super.initState();
    this.getAllCategories();
  }
  @override
  Widget build(BuildContext context) {
    return getCategoriesApiCall?Center(child: CircularProgressIndicator(),):
    SmartRefresher(
      controller: _refreshController,
      onRefresh: _onRefresh,
      child: Container(
        color:Color(0xFFF5F5F5),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(top: 16,bottom: 0,left: 16,right: 16),
                    child: Text(AppLocalizations.of(context).locale=='en'?
                    "Choose your destination":"اختر وجهتك",
                      textAlign: AppLocalizations.of(context).locale=='en'?
                      TextAlign.left:TextAlign.right,
                      style: TextStyle(
                        fontSize: 16,
                        fontFamily: "Poppins-Bold",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 12,
              fit: FlexFit.tight,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.only(left:16.0,right: 16,top: 12),
                  child: SmartRefresher(
                    controller: _refreshController,
                    onRefresh: _onRefresh,
                    child: GridView.builder(
                      itemCount: categoryList.length,
                      gridDelegate:SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 1.099,
                        crossAxisCount: 2,
                        crossAxisSpacing: 16,
                        mainAxisSpacing: 16,
                      ) ,
                      itemBuilder: (context,position){
                        return InkWell(
                          onTap:(){
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context)=>CategoryNews(categoryList[position].id,
                                    AppLocalizations.of(context).locale=='en'?categoryList[position].name:categoryList[position].name_ar)
                            ));
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.blue,
                              ),
                              /*child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    top:0,
                                    bottom: 0,right: -12,left: -12,
                                    child: Container(
                                      child: Image.network(categoryList[position].image,//Image.asset("images/cat${position+1}.png",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),*/
                              //TODO:uncomment when making API
                              child: Stack(
                              children: <Widget>[
                                Positioned(
                                  top:0,
                                  bottom: 0,
                                  left: -12,right: -12,
                                  child: Container(
                                    child: Image.asset("images/category-backgroung.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Flexible(
                                      flex: 1,
                                      fit: FlexFit.tight,
                                      child: Container(
                                      ),
                                    ),
                                    Flexible(
                                      flex: 3,
                                      fit: FlexFit.tight,
                                      child: Container(
                                        child: Center(
                                          child: Image.network(categoryList[position].image,fit: BoxFit.fitHeight,),
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      flex: 2,
                                      fit: FlexFit.tight,
                                      child: Container(
                                        child: Center(
                                          child: Text(AppLocalizations.of(context).locale=='en'?
                                          "${categoryList[position].name}":"${categoryList[position].name_ar}",
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontFamily: 'Poppins-SemiBold',
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            ),
                          ),
                        );
                      },),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}