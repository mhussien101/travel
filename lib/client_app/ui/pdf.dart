import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import '../../localizations.dart';
import 'package:path_provider/path_provider.dart';
class PdfViewer extends StatefulWidget {
  @override
  _PdfViewerState createState() => _PdfViewerState();
}

class _PdfViewerState extends State<PdfViewer> {
  String pathPDF = "";

  Future<File> createFileOfPdfUrl() async {
    // final url =
    // "https://berlin2017.droidcon.cod.newthinking.net/sites/global.droidcon.cod.newthinking.net/files/media/documents/Flutter%20-%2060FPS%20UI%20of%20the%20future%20%20-%20DroidconDE%2017.pdf";
    final url = "https://crystal-magazine.com/images/pdf/cylinder.pdf";
    final filename = url.substring(url.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  @override
  void initState() {
     createFileOfPdfUrl().then((f) {
       setState(() {
         pathPDF = f.path;
         print(pathPDF);

       });
     });
     super.initState();
  }
  @override
  Widget build(BuildContext context) {
    print("build/build");
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFF5F5F5),
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right:12.0,top: 8),
            child: Icon(Icons.search,
              size: size.width*0.09,
            ),
          ),
        ],
        backgroundColor: TravelNewsColors.blue56,
        centerTitle: true,
        title: Text(AppLocalizations.of(context).locale=='en'?'Our Magazine':"مجلتنا",
          style: TextStyle(
            fontSize: size.width*0.059,
            color: Colors.white,
          ),
        ),
      ),
      body: pathPDF != ""?PDFView(
        filePath: pathPDF,
        enableSwipe: true,
        swipeHorizontal: true,
        autoSpacing: false,
        pageFling: false,
        onRender: (_pages) {
          setState(() {
          //  pages = _pages;
           // isReady = true;
          });
        },
        onError: (error) {
          print(error.toString());
        },
        onPageError: (page, error) {
          print('$page: ${error.toString()}');
        },
        onViewCreated: (PDFViewController pdfViewController) {
        //  _controller.complete(pdfViewController);
        },
        onPageChanged: (int page, int total) {
          print('page change: $page/$total');
        },
      ) : Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
