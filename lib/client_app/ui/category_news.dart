import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:travel_app/client_app/api/category_api.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/statics/app_bar.dart';
import 'package:travel_app/client_app/statics/drawer.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/localizations.dart';
import 'news_details.dart';

class CategoryNews extends StatefulWidget{
  int id ;
  String name;
  CategoryNews(this.id,this.name);
  _CategoryNewsState createState()=> _CategoryNewsState();
}
class _CategoryNewsState extends State<CategoryNews>{
  bool popular1=false;
  bool popular2=false;
  bool popular3=false;
  bool popular4=false;
  bool popular5=false;
  bool newsApiCall=false;
  GlobalKey<ScaffoldState> _globalKeyScafoldState ;
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  List<NewsModel> newsList = List();
  List<Widget> newsItemsList= new List();
  void _onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    newsItemsList.clear();
    print("on Refresh : : ");
    this.getAllNews();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
  getAllNews(){
    setState(() {
      newsApiCall=true;
    });
    CategoryApi.getAllCategoryNewsById(widget.id).then((response){
      newsList = response.newsList;
      newsList.forEach((news)=>newsItemsList.add(NewsItem(news)));
      setState(() {
        newsApiCall=false;
      });
    },onError: (error){
      setState(() {
        newsApiCall=false;
      });
      print("get All category news error : :  $error");
    });
  }
  @override
  void initState() {
    super.initState();
    _globalKeyScafoldState = new GlobalKey<ScaffoldState>();
    this.getAllNews();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerClass().showDrawer(context),
      key: _globalKeyScafoldState,
      appBar: new AppBarClass().appBar(context ,_globalKeyScafoldState,title: '${widget.name?? 'Category'} '),
      body:newsApiCall?Center(child: CircularProgressIndicator(),):
      SmartRefresher(
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            padding: EdgeInsets.only(top: 16,bottom: 16),
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: newsItemsList,
            ),
          ),
        ),
      ),
    );
  }
}
class NewsItem extends StatefulWidget{
  NewsModel news ;
  NewsItem(this.news);
  _NewsItemState createState()=> _NewsItemState();
}
class _NewsItemState extends State<NewsItem>{
  bool popular1=false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
            builder: (context)=>NewsDetails(widget.news.id)
        ));
      },
      child:Container(
        padding: EdgeInsets.only(left: 16,right: 16,bottom: 16),
        height: MediaQuery.of(context).size.width<415?170:MediaQuery.of(context).size.height/4.2,
        child: ClipRRect(
          child: Container(
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: Image.network(widget.news.image,fit: BoxFit.fill,),//Image.asset("images/newsCat1.png",fit: BoxFit.fill,),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomRight,
                      end: Alignment.topRight,
                      stops: [0.1, 0.4,0.6],
                      colors: [
                        Colors.black87,
                        Colors.black45,
                        Colors.black12,
                      ],
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(8),
                          child: Align(
                            alignment: AppLocalizations.of(context).locale=='en'?
                            Alignment.topRight:Alignment.topLeft,
                            child: InkWell(
                                onTap: (){
                                  setState(() {
                                    popular1=!popular1;
                                  });
                                },
                                child: popular1?Image.asset("images/home_popular_save_active.png",width: 24,height: 24,)
                                    :Image.asset("images/home_popular_save.png",width: 24,height: 24,)
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:16.0,bottom: 2,right: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding:EdgeInsets.only(top: 4,bottom: 4,left: 8,right: 8),
                              decoration: BoxDecoration(
                                color: TravelNewsColors.blue56,
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: Center(
                                child: Text("${widget.news.news_paper}",//"CNN",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins-SemiBold",
                                    fontSize: MediaQuery.of(context).size.width<365?8:10,
                                  ),
                                ),
                              ),
                            ),
                            Text("    ${widget.news.created_at.substring(0,11)}",//September 2,2019",
                              style: TextStyle(
                                fontSize: MediaQuery.of(context).size.width<365?8:10,
                                color: Colors.white,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:16.0,bottom: 16,right: 16),
                        child: Text(AppLocalizations.of(context).locale=='en'?
                        "${widget.news.name}":"${widget.news.name_ar}",//"10 PLACES EVERYONE WANTS TO TRAVEL TO IN 2019",
                          textAlign:AppLocalizations.of(context).locale=='en'?
                          TextAlign.left:TextAlign.right,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width<365?15:20,
                            fontFamily: "Poppins-Bold",
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }

}