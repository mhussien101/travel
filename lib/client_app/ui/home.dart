import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:travel_app/client_app/api/home_api.dart';
import 'package:travel_app/client_app/model/NewsDatabaseProvider.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/client_app/ui/news_details.dart';
import 'package:travel_app/localizations.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  bool popular1 = false;
  bool popular2 = false;
  bool new1 = false;
  bool new2 = false;
  bool new3 = false;
  bool getNewsApi = false;

  List<NewsModel> latest = List();
  List<NewsModel> popular = List();
  List<Widget> carouselPopularItems = new List();
  List<Widget> latestItemsList = new List();

  void _onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    latestItemsList.clear();
    print("on Refresh : : ");
    this.getHomeNews();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  getHomeNews() {
    setState(() {
      getNewsApi = true;
    });
    HomeApi.getHomeNews().then((response) {
      TodoProvider databaseProvider = new TodoProvider();
      databaseProvider.open().then((onValue) {
        latest = response.latestList;
        latest.forEach(
            (latest) => databaseProvider.getTodo(latest.id).then((onValue) {
                  if (onValue == 1) {
                    print("latest.id/..." + latest.id.toString());
                    latest.isSaved = true;
                    latestItemsList.add(LatestItem(latest));
                    setState(() {

                    });
                  } else {
                    latest.isSaved = false;
                    latestItemsList.add(LatestItem(latest));
                    setState(() {

                    });
                  }
                }, onError: (error) {
                  print("latest.onError/..." + error);
                  {
                    latest.isSaved = false;
                    latestItemsList.add(LatestItem(latest));
                    setState(() {

                    });
                  }
                }));

        popular = response.popularList;
        popular.forEach(
            (popular) => databaseProvider.getTodo(popular.id).then((onValue) {
                  if (onValue == 1) {
                    popular.isSaved = true;
                    carouselPopularItems.add(PopularItem(popular));
                    setState(() {

                    });
                  } else {
                    popular.isSaved = false;
                    carouselPopularItems.add(PopularItem(popular));
                    setState(() {

                    });
                  }
                }, onError: (error) {
                  {
                    popular.isSaved = false;
                    carouselPopularItems.add(PopularItem(popular));
                    setState(() {

                    });
                  }
                }));
        setState(() {
          getNewsApi = false;
        });
        print("get news done !!");
        setState(() {});
      });
    }, onError: (error) {
      setState(() {
        getNewsApi = false;
        print("get news error :: :  $error");
      });
    });
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    this.getHomeNews();
  }

  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width);
    print(MediaQuery.of(context).size.height);
    return getNewsApi
        ? Center(
            child: CircularProgressIndicator(),
          )
        : SmartRefresher(
            controller: _refreshController,
            //enablePullDown: true,
            //enablePullUp: true,
            onRefresh: _onRefresh,
            child: SingleChildScrollView(
              child: Container(
                  color: Color(0xFFF5F5F5),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height < 685
                            ? 210
                            : MediaQuery.of(context).size.height / 3.5,
                        child: Column(
                          children: <Widget>[
                            Flexible(
                              fit: FlexFit.tight,
                              flex: 1,
                              child: Container(
                                padding: EdgeInsets.only(left: 16, right: 16),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      AppLocalizations.of(context).locale ==
                                              'en'
                                          ? "Popular"
                                          : 'الشائع',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "Poppins-Bold",
                                        color: Colors.black,
                                      ),
                                    ),
                                    Text(
                                      AppLocalizations.of(context).locale ==
                                              'en'
                                          ? "See all"
                                          : 'رؤية الكل',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: "Poppins-Medium",
                                        color: TravelNewsColors.blue56,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Flexible(
                              fit: FlexFit.tight,
                              flex: 3,
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                child: CarouselSlider(
                                  items: carouselPopularItems.length > 0
                                      ? carouselPopularItems
                                      : [Container()],
                                  //autoPlayCurve: Curves.easeInOutCubic,
                                  //height: 400,
                                  viewportFraction: .65,
                                  // height:300,// MediaQuery.of(context).size.height/2,
                                  autoPlay: true,
                                  enableInfiniteScroll: true,
                                  scrollDirection: Axis.horizontal,
                                  pauseAutoPlayOnTouch: Duration(seconds: 3),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:
                                  EdgeInsets.only(left: 16, right: 16, top: 16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    AppLocalizations.of(context).locale == 'en'
                                        ? "New"
                                        : 'أخبار جديدة',
                                    style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width <
                                                  340
                                              ? 14
                                              : 16,
                                      fontFamily: "Poppins-Bold",
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    AppLocalizations.of(context).locale == 'en'
                                        ? "See all"
                                        : 'رؤية الكل',
                                    style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.width <
                                                  340
                                              ? 14
                                              : 16,
                                      fontFamily: "Poppins-Medium",
                                      color: TravelNewsColors.blue56,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  left: 16, right: 16, bottom: 4),
                              child: Column(
                                children: latestItemsList,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          );
  }
}

class PopularItem extends StatefulWidget {
  NewsModel news;

  PopularItem(this.news);

  _PopularItemState createState() => _PopularItemState();
}

class _PopularItemState extends State<PopularItem> {
  bool popular1 = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NewsDetails(widget.news.id)));
        },
        child: ClipRRect(
          child: Container(
            height: 120,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: Image.network(widget.news.image,
                      fit: BoxFit
                          .fill), //Image.asset("images/caro2.png",fit: BoxFit.fill,),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomRight,
                      end: Alignment.topRight,
                      stops: [0.1, 0.4, 0.6],
                      colors: [
                        Colors.black87,
                        Colors.black45,
                        Colors.black12,
                      ],
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(8),
                          child: Align(
                            alignment:
                                AppLocalizations.of(context).locale == 'en'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                            child: InkWell(
                                onTap: () {
                                  TodoProvider databaseProvider =
                                      new TodoProvider();
                                  databaseProvider.open().then((response) {
                                    if (widget.news.isSaved) {
                                      print("mkonmjnolkinmjkn/ddd" +
                                          widget.news.id.toString());
                                      databaseProvider
                                          .delete(widget.news.id)
                                          .then((onValue) {
                                        setState(() {
                                          widget.news.isSaved = false;
                                          popular1 = popular1;
                                        });
                                      }, onError: (error) {
                                        print(
                                            " delete from database error :: :  $error");
                                      });
                                    } else {
                                      print("mkonmjnolkinmjkn/ddd" +
                                          widget.news.id.toString());
                                      databaseProvider.insert(widget.news).then(
                                          (onValue) {
                                        setState(() {
                                          widget.news.isSaved = true;
                                          popular1 = !popular1;
                                        });
                                      }, onError: (error) {
                                        print(
                                            " insert database error :: :  $error");
                                      });
                                    }
                                  }, onError: (error) {
                                    print(" open database error :: :  $error");
                                  });
                                },
                                child: widget.news.isSaved
                                    ? Image.asset(
                                        "images/home_news_save_active.png",
                                        width: 24,
                                        height: 24,
                                      )
                                    : Image.asset(
                                        "images/home_news_save.png",
                                        width: 24,
                                        height: 24,
                                      )),
                          ),
                        ),
                      ),
                      Padding(
                        padding: AppLocalizations.of(context).locale == 'en'
                            ? EdgeInsets.only(left: 16.0, bottom: 2)
                            : EdgeInsets.only(right: 16, bottom: 2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: 4, bottom: 4, left: 8, right: 8),
                              decoration: BoxDecoration(
                                color: TravelNewsColors.blue56,
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: Center(
                                child: Text(
                                  "${widget.news.news_paper}",
                                  //"CNN",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins-SemiBold",
                                    fontSize:
                                        MediaQuery.of(context).size.width < 340
                                            ? 8
                                            : 10,
                                  ),
                                ),
                              ),
                            ),
                            Text(
                              "      ${widget.news.created_at.substring(0, 11)}", //September 2,2019",
                              style: TextStyle(
                                fontSize: 10,
                                color: Colors.white,
                                fontFamily: "Poppins-Regular",
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, bottom: 16, right: 16),
                        child: Text(
                          AppLocalizations.of(context).locale == 'en'
                              ? "${widget.news.name}"
                              : widget.news.name_ar,
                          //"10 PLACES EVERYONE WANTS TO TRAVEL TO IN 2019",
                          textAlign: AppLocalizations.of(context).locale == 'en'
                              ? TextAlign.left
                              : TextAlign.right,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width < 340
                                ? 12
                                : 18,
                            fontFamily: "Poppins-Bold",
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}

class LatestItem extends StatefulWidget {
  NewsModel news;

  LatestItem(this.news);

  _LatestItemState createState() => _LatestItemState();
}

class _LatestItemState extends State<LatestItem> {
  bool new1 = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NewsDetails(widget.news.id)));
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              height: MediaQuery.of(context).size.width < 365 ? 120 : 150,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height:
                            MediaQuery.of(context).size.width < 365 ? 120 : 150,
                        child: Image.network(widget.news.image,
                            fit: BoxFit
                                .cover), //Image.asset("images/listhome1.png",fit: BoxFit.cover,),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Flexible(
                            fit: FlexFit.tight,
                            flex: 3,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      AppLocalizations.of(context).locale ==
                                              'en'
                                          ? const EdgeInsets.only(
                                              left: 8.0, bottom: 0, top: 8)
                                          : EdgeInsets.only(right: 8, top: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: 2,
                                            bottom: 0,
                                            left: 6,
                                            right: 6),
                                        decoration: BoxDecoration(
                                          color: TravelNewsColors.blue56,
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: Center(
                                          child: Text(
                                            "${widget.news.news_paper}",
                                            //"CNN",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "Poppins-SemiBold",
                                              fontSize: MediaQuery.of(context)
                                                          .size
                                                          .width <
                                                      365
                                                  ? 8
                                                  : 10,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "    ${widget.news.created_at.substring(0, 11)}", //September 2,2019",
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width <
                                                  365
                                              ? 8
                                              : 10,
                                          color: Colors.black,
                                          fontFamily: "Poppins-Regular",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: AppLocalizations.of(context)
                                                .locale ==
                                            'en'
                                        ? EdgeInsets.only(top: 8.0, right: 8)
                                        : EdgeInsets.only(top: 8, left: 8),
                                    child: Align(
                                      alignment:
                                          AppLocalizations.of(context).locale ==
                                                  'en'
                                              ? Alignment.topRight
                                              : Alignment.topLeft,
                                      child: InkWell(
                                          onTap: () {
                                            TodoProvider databaseProvider =
                                                new TodoProvider();
                                            databaseProvider.open().then(
                                                (response) {
                                              if (widget.news.isSaved) {
                                                print("mkonmjnolkinmjkn/ddd" +
                                                    widget.news.id.toString());
                                                databaseProvider
                                                    .delete(widget.news.id)
                                                    .then((onValue) {
                                                  setState(() {
                                                    widget.news.isSaved = false;
                                                    new1 = new1;
                                                  });
                                                }, onError: (error) {
                                                  print(
                                                      " delete from database error :: :  $error");
                                                });
                                              } else {
                                                print("mkonmjnolkinmjkn/ddd" +
                                                    widget.news.id.toString());
                                                databaseProvider
                                                    .insert(widget.news)
                                                    .then((onValue) {
                                                  setState(() {
                                                    widget.news.isSaved = true;
                                                    new1 = !new1;
                                                  });
                                                }, onError: (error) {
                                                  print(
                                                      " insert database error :: :  $error");
                                                });
                                              }
                                            }, onError: (error) {
                                              print(
                                                  " open database error :: :  $error");
                                            });
                                          },
                                          child: widget.news.isSaved
                                              ? Image.asset(
                                                  "images/home_news_save_active.png",
                                                  width: 24,
                                                  height: 24,
                                                )
                                              : Image.asset(
                                                  "images/home_news_save.png",
                                                  width: 24,
                                                  height: 24,
                                                )),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 3,
                            fit: FlexFit.loose,
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8.0, top: 2, right: 8),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        child: Text(
                                          AppLocalizations.of(context).locale ==
                                                  'en'
                                              ? "${widget.news.name}"
                                              : "${widget.news.name_ar}",
                                          textAlign:
                                              AppLocalizations.of(context)
                                                          .locale ==
                                                      'en'
                                                  ? TextAlign.left
                                                  : TextAlign.right,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          //"Maui, Hawaii — September is the cheapest time to go",
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context)
                                                        .size
                                                        .width <
                                                    365
                                                ? 12
                                                : 16,
                                            fontFamily: "Poppins-Bold",
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 6,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 8.0, top: 0, right: 8, bottom: 2),
                              child: Text(
                                AppLocalizations.of(context).locale == 'en'
                                    ? "${widget.news.desc}"
                                    : "${widget.news.desc_ar}",
                                textAlign:
                                    AppLocalizations.of(context).locale == 'en'
                                        ? TextAlign.left
                                        : TextAlign.right,
                                style: TextStyle(
                                  fontSize:
                                      MediaQuery.of(context).size.width < 365
                                          ? 10
                                          : 12,
                                  fontFamily: "Poppins-Regular",
                                ),
                                maxLines:
                                    MediaQuery.of(context).size.width < 340
                                        ? 2
                                        : 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
