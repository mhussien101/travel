
import 'package:flutter/material.dart';
import 'package:travel_app/client_app/api/magazine_api.dart';
import 'package:travel_app/client_app/model/news_model.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';
import 'package:travel_app/client_app/ui/pdf.dart';
import '../../localizations.dart';

class Magazine extends StatefulWidget {
  _MagazineState createState() => _MagazineState();
}

class _MagazineState extends State<Magazine> {
  List<Widget> latestItemsList= new List();

  void onRefresh() {
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    latestItemsList.clear();
    print("on Refresh : : ");
    this.getMagazines();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: Color(0xFFF5F5F5),
        appBar: AppBar(
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right:12.0,top: 8),
              child: Icon(Icons.search,
                size: size.width*0.09,
              ),
            ),
          ],
          backgroundColor: TravelNewsColors.blue56,
          centerTitle: true,
          title: Text(AppLocalizations.of(context).locale=='en'?'Our Magazine':"مجلتنا",
            style: TextStyle(
              fontSize: size.width*0.059,
              color: Colors.white,
            ),
          ),
        ),
      body: Container(
        color: Color(0xFFF5F5F5),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:SingleChildScrollView(
          child: Container(
              color: Color(0xFFF5F5F5),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only( top: 16),
                    child: Column(
                      children: <Widget>[

                        Container(
                          padding: EdgeInsets.only(left:16,right: 16,bottom: 16, top: 16),
                          child: Column(
                            children: latestItemsList,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
          ),
        ),
      ),
    );

  }
  @override
  void initState() {
    super.initState();
    this.getMagazines();
  }

  getMagazines() {
    MagazineApi.getHomeNews().then((response){
        print("getSavedData/response/length" + response.pdfs.length.toString());
        response.pdfs.forEach((pdfs)=>latestItemsList.add(MagazineItem(pdfs)));
        print("bjknlgtmjnjkgnkbg");
        setState(() {});
      },onError: (error){
        print("get saved news error :: :  $error");
      });
  }
}

class MagazineItem extends StatefulWidget {
  int position;
  MagazineModel magazines;

  MagazineItem(this.magazines);

  @override
  _SavedItemState createState() => _SavedItemState();
}

class _SavedItemState extends State<MagazineItem> {
  bool new1 = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: InkWell(
        onTap: (){
          // TODO : download pfd
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PdfViewer()));
        },
        child: ClipRRect(
          borderRadius:BorderRadius.circular(4),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              height: MediaQuery.of(context).size.width<365?80:100,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height: MediaQuery.of(context).size.width<365?80:100,
                        child: Image.network(widget.magazines.image,fit: BoxFit.cover),//Image.asset("images/listhome1.png",fit: BoxFit.cover,),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      child: Column(
                        children: <Widget>[

                          Flexible(
                            flex: 3,
                            fit: FlexFit.loose,
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(left:8.0,top: 2,right: 8.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        child: Text(AppLocalizations.of(context).locale=='en'?
                                        "${widget.magazines.name}":"${widget.magazines.name_ar}",
                                          textAlign: AppLocalizations.of(context).locale=='en'?
                                          TextAlign.left:TextAlign.right,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,//"Maui, Hawaii — September is the cheapest time to go",
                                          style: TextStyle(
                                            fontSize: MediaQuery.of(context).size.width<365?14:18,
                                            fontFamily: "Poppins-Bold",
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            flex:3,
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left:8.0,top: 2,right: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text("    ${widget.magazines.date}",//September 2,2019",
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context).size.width<365?10:12,
                                          color: Colors.black,
                                          fontFamily: "Poppins-Regular",
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}