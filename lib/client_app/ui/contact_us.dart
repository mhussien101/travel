import 'package:flutter/material.dart';
import 'package:travel_app/client_app/statics/travel_news_colors.dart';

import '../../localizations.dart';
class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFF5F5F5),
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: AppLocalizations.of(context).locale=='en'?
            EdgeInsets.only(right:12.0,top: 8):EdgeInsets.only(left: 16,top: 8),
            child: Icon(Icons.search,
              size: size.width*0.09,
            ),
          ),
        ],
        backgroundColor: TravelNewsColors.blue56,
        centerTitle: true,
        title: Text(AppLocalizations.of(context).locale=='en'?
        'Contact Us':"اتصل بنا",
          style: TextStyle(
            fontSize: size.width*0.059,
            color: Colors.white,
          ),
        ),
      ),
      body:SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top :4.0),
                  child: Container(
                    height: size.height/3.5,
                    width: size.width/1.5,
                    child: Image.asset("images/contact.png",fit: BoxFit.fill,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:32.0),
                  child: Container(
                    width: size.width,
                    child: Text(AppLocalizations.of(context).locale=='en'?"You can contact "
                    "with us at any time":"يمكنك التواصل معنا فى أى وقت",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: size.width*0.065,
                          fontFamily:'Poppins-Bold'
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:14.0),
                  child: Container(
                    width: size.width,
                    child: Text(AppLocalizations.of(context).locale=='en'?
                    "social media : ":"التواصل الإجتماعى: ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: TravelNewsColors.grey7E,
                        fontSize: size.width*0.046,
                        fontFamily:'Poppins-Medium',
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:16.0),
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                          child: Container(),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right :20.0),
                          child: Container(
                            child: Image.asset("images/face.png",height: 32,width: 32,),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right :20.0),
                          child: Container(
                            child: Image.asset("images/twitter.png",height: 32,width: 32,),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right :20.0),
                          child: Container(
                            child: Image.asset("images/instagram.png",height: 32,width: 32,),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right :20.0),
                          child: Container(
                            child: Image.asset("images/linkedin.png",height: 32,width: 32,),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right:20.0),
                          child: Container(
                            child: Image.asset("images/youtube.png",height: 32,width: 32,),
                          ),
                        ),
                        Expanded(
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:32.0),
                  child: Text(AppLocalizations.of(context).locale=='en'?
                  "or you can call us on: ":"يمكنك الإتصال بنا على",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: TravelNewsColors.grey7E,
                      fontSize: size.width*0.046,
                      fontFamily:'Poppins-Medium',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:14.0),
                  child: Container(
                    width: size.width,
                    child: Text("01027444488",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: size.width*0.065,
                          fontFamily:'Poppins-Bold'
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:32.0),
                  child: Text(AppLocalizations.of(context).locale=='en'?
                  "Our location:":"عنواننا",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: TravelNewsColors.grey7E,
                      fontSize: size.width*0.046,
                      fontFamily:'Poppins-Medium',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:14.0),
                  child: Container(
                    width: size.width,
                    child: Text(AppLocalizations.of(context).locale=='en'?"6 Mostafa Riyad ST Al Manteqah"
                      "Al Oula Nasr City Cairo Egypt":"6 شارع مصطفى رياض، المنطقة الأولى مدينة نصر القاهرة، مصر",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: size.width*0.065,
                          fontFamily:'Poppins-Bold'
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
