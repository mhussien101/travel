import 'package:shared_preferences/shared_preferences.dart';
class User {
  //for app language
  String locale ;
  // for driver

  User({this.locale});
}

class UserLocalStorage {
  static Future<bool> logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear() ;
    return true ;
  }
  static Future<bool> logIn(User user) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString("locale", user.locale);
      print("locale saved : : :  ${user.locale}");
      return true ;
    }catch(Excption){
      print("save to shared faild   :  $Excption");
      return false ;
    }
  }
  static Future< User > getUser () async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return new User(
      locale: prefs.getString("locale"),
    );
  }
}