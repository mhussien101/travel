import 'package:travel_app/client_app/model/tags_model.dart';

final String tableTodo = 'todo';
final String columnId = 'id';
final String columncatId = 'cat_id';
final String columnCreatedAt = 'created_at';
final String columnDesc = 'desc';
final String columnDescAr = 'desc_ar';
final String columnImage = 'image';
final String columnName = 'name';
final String columnNameAr = 'name_ar';
final String columnNewsPaper = 'news_paper';
final String columnNewsUpdatedAt = 'updated_at';
final String columnNewsView = 'view';

class NewsModel {
    int cat_id;
    String created_at;
    String desc;
    String desc_ar;
    int id;
    String image;
    bool isSaved = true;
    String name;
    String name_ar;
    String news_paper;
    String updated_at;
    int view;

    NewsModel({this.cat_id, this.created_at, this.desc, this.desc_ar, this.id, this.image, this.name, this.name_ar, this.news_paper, this.updated_at, this.view});

    // convert json to NewsModel
    factory NewsModel.fromJson(Map<String, dynamic> json) {
        return NewsModel(
            cat_id: json['cat_id'],
            created_at: json['created_at'],
            desc: json['desc'] ,
            desc_ar: json['desc_ar'],
            id: json['id'],
            image: json['image'],
            name: json['name'],
            name_ar: json['name_ar'],
            news_paper: json['news_paper'],
            updated_at: json['updated_at'],
            view: json['view'],
        );
    }

    // convert NewsModel to json
    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['cat_id'] = this.cat_id;
        data['created_at'] = this.created_at;
        data['id'] = this.id;
        data['image'] = this.image;
        data['name'] = this.name;
        data['name_ar'] = this.name_ar;
        data['news_paper'] = this.news_paper;
        data['updated_at'] = this.updated_at;
        data['view'] = this.view;
        data['desc'] = this.desc;
        data['desc_ar'] = this.desc_ar;
        return data;
    }
}
class HomeServerResponse{
  List<NewsModel> latestList;
  List<NewsModel> popularList;
  HomeServerResponse({this.latestList,this.popularList});
  factory HomeServerResponse.fromJson(Map<String, dynamic> json) {
    return HomeServerResponse(
      latestList: json['latest'] != null ? (json['latest'] as List).map((i) => NewsModel.fromJson(i)).toList() : null,
      popularList: json['popular'] != null ? (json['popular'] as List).map((i) => NewsModel.fromJson(i)).toList() : null,
    );
  }
}

class MagazineModel {
  String date;
  int id;
  String image;
  String pdf;
  String name;
  String name_ar;
  int view;

  MagazineModel({ this.id, this.image, this.name, this.name_ar,this.pdf,this.date, this.view});

  // convert json to NewsModel
  factory MagazineModel.fromJson(Map<String, dynamic> json) {
    return MagazineModel(
      id: json['id'],
      image: json['image'],
      name: json['name'],
      name_ar: json['name_ar'],
      pdf: json['pdf'],
      date: json['date'],
      view: json['view'],
    );
  }

  // convert NewsModel to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['name'] = this.name;
    data['name_ar'] = this.name_ar;
    data['pdf'] = this.pdf;
    data['date'] = this.date;
    data['view'] = this.view;
    return data;
  }
}

class MagazineServerResponse{
  List<MagazineModel> pdfs;
  MagazineServerResponse({this.pdfs});
  factory MagazineServerResponse.fromJson(Map<String, dynamic> json) {
    return MagazineServerResponse(
      pdfs: json['pdfs'] != null ? (json['pdfs'] as List).map((i) => MagazineModel.fromJson(i)).toList() : null
    );
  }
}

class TrendServerResponse{
  List<NewsModel> trendingList;
  TrendServerResponse({this.trendingList});
  factory TrendServerResponse.fromJson(Map<String, dynamic> json) {
    return TrendServerResponse(
      trendingList: json['tranding'] != null ? (json['tranding'] as List).map((i) => NewsModel.fromJson(i)).toList() : null,
    );
  }
}

class CategoryNewsByIdResponse{
  List<NewsModel> newsList;
  CategoryNewsByIdResponse({this.newsList});
  factory CategoryNewsByIdResponse.fromJson(Map<String, dynamic> json) {
    return CategoryNewsByIdResponse(
      newsList: json['News'] != null ? (json['News'] as List).map((i) => NewsModel.fromJson(i)).toList() : null,
    );
  }
}

class NewsDetailsServerResponse{
  NewsModel news;
  List<NewsModel> similarList = List() ;
  List<TagsModel> tagsList = List();
  NewsDetailsServerResponse({this.news,this.tagsList,this.similarList});
  factory NewsDetailsServerResponse.fromJson(Map<String, dynamic> json) {
    return NewsDetailsServerResponse(
      news: NewsModel.fromJson(json['New']),
      tagsList: json['tags'] != null ? (json['tags'] as List).map((i) => TagsModel.fromJson(i)).toList() : [],
      similarList: json['similar'] != null ? (json['similar'] as List).map((i) => NewsModel.fromJson(i)).toList() : [],

    );
  }
}
