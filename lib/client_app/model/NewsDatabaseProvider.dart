import 'package:sqflite/sqflite.dart';

import 'news_model.dart';

class TodoProvider {
  Database db;

  Future open() async {
    var databasesPath = await getDatabasesPath();
    String path = databasesPath + tableTodo;
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableTodo ( 
  $columnId integer primary key, 
  $columncatId integer not null,
  $columnCreatedAt text,
  $columnDesc text,
  $columnDescAr text,
  $columnImage text,
  $columnName text,
  $columnNameAr text,
  $columnNewsPaper text,
  $columnNewsUpdatedAt text,
  $columnNewsView integer)
''');
    });
  }

  Future<NewsModel> insert(NewsModel todo) async {
    if (db != null) {
      print("mkonmjnolkinmjkn" + todo.id.toString());
      todo.id = await db.insert(tableTodo, todo.toJson());
    }
    return todo;
  }

  Future<List<NewsModel>> getSavedData() async {
    if (db != null) {
      List<Map> list = await db.rawQuery('SELECT * FROM $tableTodo');
      print("getSavedData/befour/length" + list.length.toString());

      List<NewsModel> news = new List<NewsModel>();
      for (var i = 0; i <= list.length -1; i++) {
        news.add(NewsModel.fromJson(list.elementAt(i)));
        print("getSavedData/getItemId/.. " + news.elementAt(i).id.toString());
      }
      print("getSavedData/after/length/. " + news.length.toString());

      return news;
    } else
      {
        print("getSavedData/error");
        return  null;
      }
  }

  Future<int> getTodo(int id) async {
    print("getTodo/mkmk/.." + id.toString());
    List<Map> maps = await db.query(tableTodo,
        columns: [columnId],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      print("njklmbgkvlngklbnv/mkmk");
      return 1;
    }
    print("njklmbgkvlngklbnv");
    return 0;
  }

  Future<int> delete(int id) async {
    return await db.delete(tableTodo, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(NewsModel todo) async {
    return await db.update(tableTodo, todo.toJson(),
        where: '$columnId = ?', whereArgs: [todo.id]);
  }

  Future close() async => db.close();
}
