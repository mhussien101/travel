class CategoryModel {
    String created_at;
    int id;
    String image;
    String name;
    String name_ar;
    int sort;
    String updated_at;

    CategoryModel({this.created_at, this.id, this.image, this.name, this.name_ar, this.sort, this.updated_at});

    factory CategoryModel.fromJson(Map<String, dynamic> json) {
        return CategoryModel(
            created_at: json['created_at'] ,
            id: json['id'],
            image: json['image'],
            name: json['name'],
            name_ar: json['name_ar'],
            sort: json['sort'],
            updated_at: json['updated_at'],
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['id'] = this.id;
        data['image'] = this.image;
        data['name'] = this.name;
        data['name_ar'] = this.name_ar;
        data['sort'] = this.sort;
        data['created_at'] = this.created_at;
        data['updated_at'] = this.updated_at;
        return data;
    }
}
class GetAllCategoryServerResponse{
    List<CategoryModel> categoryList;
    GetAllCategoryServerResponse({this.categoryList});
    factory GetAllCategoryServerResponse.fromJson(Map<String, dynamic> json) {
        return GetAllCategoryServerResponse(
            categoryList: json['Category'] != null ? (json['Category'] as List).map((i) => CategoryModel.fromJson(i)).toList() : null,
        );
    }
}
