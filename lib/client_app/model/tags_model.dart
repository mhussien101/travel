class TagsModel {
  String created_at;
  int id;
  String name;
  String name_ar;
  String updated_at;

  TagsModel({this.created_at, this.id, this.name, this.name_ar, this.updated_at});

  factory TagsModel.fromJson(Map<String, dynamic> json) {
    return TagsModel(
      created_at: json['created_at'],
      id: json['id'],
      name: json['name'],
      name_ar: json['name_ar'],
      updated_at: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.created_at;
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.name_ar;
    data['updated_at'] = this.updated_at;
    return data;
  }
}